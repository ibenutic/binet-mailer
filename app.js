const express = require('express')
require('dotenv').config({ path: '.env' })
const bodyParser = require('body-parser')
const nodemailer = require('nodemailer')

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))




const mailer = nodemailer.createTransport({
    host: process.env.HOST,
    port: process.env.MAIL_PORT,
    secure: true,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.PASS
    },
    tls: {
        rejectUnauthorized: false
    }

})

// app.use((req,res,next) => {
//     res.setHeader('Access-Control-Allow-Origin','*')
//     res.setHeader('Access-Control-Allow-Methods','GET, PUT, POST, PATCH, DELETE')
//     res.setHeader('Access-Control-Allow-Headers','Content-Type,Authorization')
//     next()
// })

app.get('/client',(req,res) => {
    res.status(200).json({success:true})
})


app.post('/client', (req, res) => {
    if(req.body.key !== process.env.KEY) {
        res.status(403).json({error:'Key invalid'})
        return null
    }
    mailer.sendMail({
        from: req.body.from,
        to: req.body.to,
        subject: req.body.subject,
        html: req.body.msg
    }, (err, info) => {
        if (err) return res.status(500).send(err)
        res.json({ success: true })
    })
})

app.get('/client',(req,res) => {
    console.log('taken');
    res.status(200).send('Wowkin')
})


app.listen(process.env.PORT)
console.log(`Working on port ${process.env.PORT}`);
